# Test case

![Test case](/task.png)

## Build and Run

### Fedora 27/28/29

```shell
dnf install gcc-c++ cmake make -y
```

#### Build

```shell
cd test-pro
mkdir build
cd build
cmake ..
make -j$(nproc)
```

### Run

```shell
Usage: test-pro [<switches>...] [<options>...]

<switches>
    -s/-c   : Operational mode (server/client)
    -t/-u   : Protocol to communicate (tcp/udp)
<options>
    -a [ip] : IP address to bind in server mode or to connect to in client one (default: 127.0.0.1)
    -p [num]: Port number to listen in server mode or to connect to in client one (default: 5555)
```
