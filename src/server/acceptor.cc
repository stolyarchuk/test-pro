#include "acceptor.h"

#include <arpa/inet.h>
#include <netinet/in.h>

Acceptor::Acceptor(Io& io, const Options& opts) : io_(io), socket_(io) {
  socket_.Open();
  socket_.SetNonBlocking();

  addr_.sin_family = AF_INET;
  addr_.sin_port = htons(opts.GetPort());

  if (inet_pton(AF_INET, opts.GetIP().c_str(), &addr_.sin_addr) <= 0)
    throw std::runtime_error("Bad address");
}

Acceptor::~Acceptor() {
  socket_.Close();
}

void Acceptor::Bind() {
  if (bind(socket_.Raw(), (sockaddr*)&addr_, sizeof(addr_)) < 0)
    throw std::runtime_error("Bind failed");
}

void Acceptor::Listen() {
  if (listen(socket_.Raw(), 1024) < 0)
    throw std::runtime_error("Listen failed");
}

void Acceptor::AsyncAccept(AcceptHandler::Func&& f) {
  auto handler = std::make_shared<AcceptHandler>(std::move(f));

  handler->event.events = EPOLLIN | EPOLLET;
  handler->event.data.fd = socket_.Raw();

  io_.Register(handler);
}
