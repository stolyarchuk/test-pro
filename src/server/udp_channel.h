#ifndef UDP_CHANNEL_H
#define UDP_CHANNEL_H

#include <netinet/ip.h>
#include <sys/socket.h>

#include "base_channel.h"
#include "common/io.h"
#include "common/options.h"
#include "common/parser.h"
#include "common/udp_socket.h"

class UdpChannel : public Channel {
 public:
  using Ptr = std::unique_ptr<UdpChannel>;

  UdpChannel(Io&, const Options&);

  void Bind();
  void Start() override;

 private:
  Io& io_;
  UdpSocket socket_;
  sockaddr_in addr_;
  Parser parser_;

  void Reply(const std::string&, const sockaddr* client, socklen_t socklen);
};

#endif  // UDP_CHANNEL_H
