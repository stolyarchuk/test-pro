#include "server_mode.h"

ServerMode::ServerMode(Io& io, const Options& opts) {
  switch (opts.GetProto()) {
    case Proto::TCP:
      channel_ = std::make_unique<TcpChannel>(io, opts);
      break;
    case Proto::UDP:
      channel_ = std::make_unique<UdpChannel>(io, opts);
      break;
    case Proto::NONE:
      break;
  }
}

void ServerMode::Start() {
  if (channel_)
    channel_->Start();
}
