#ifndef SERVER_H
#define SERVER_H

#include <memory>

#include "common/base_mode.h"
#include "common/options.h"
#include "common/signal_set.h"
#include "tcp_channel.h"
#include "udp_channel.h"

class ServerMode : public BaseMode {
 public:
  using Ptr = std::unique_ptr<ServerMode>;
  ServerMode(Io&, const Options&);

  void Start() override;

 private:
  Channel::Ptr channel_;
};

#endif  // SERVER_H
