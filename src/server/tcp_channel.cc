#include "tcp_channel.h"

#include <arpa/inet.h>

#include "session.h"

TcpChannel::TcpChannel(Io& io, const Options& opts) : io_(io), acceptor_(io, opts) {
  acceptor_.Bind();
  acceptor_.Listen();
}

void TcpChannel::Start() {
  acceptor_.AsyncAccept([&](int socket_fd) {
    struct sockaddr_in addr;
    socklen_t addr_size = sizeof(struct sockaddr_in);

    if (!getpeername(socket_fd, (struct sockaddr*)&addr, &addr_size))
      std::cout << "New connection from " << inet_ntoa(addr.sin_addr) << ':' << htons(addr.sin_port)
                << std::endl;

    TcpSocket socket(io_, socket_fd);
    socket.SetNonBlocking();

    auto session = std::make_shared<Session>(io_, std::move(socket));
    session->Start();
  });
}
