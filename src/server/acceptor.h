#ifndef ACCEPTOR_H
#define ACCEPTOR_H

#include <netinet/ip.h>
#include <sys/socket.h>

#include "common/io.h"
#include "common/options.h"
#include "common/tcp_socket.h"

class Acceptor {
 public:
  Acceptor(Io& io, const Options&);
  ~Acceptor();

  void Bind();
  void Listen();

  void AsyncAccept(AcceptHandler::Func&& f);

 private:
  Io& io_;
  TcpSocket socket_;

  sockaddr_in addr_;
};

#endif  // ACCEPTOR_H
