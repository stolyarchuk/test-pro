#include "session.h"

#include <algorithm>
#include <numeric>
#include <regex>
#include <sstream>

Session::Session(Io& io, TcpSocket&& socket) : io_(io), socket_(std::move(socket)) {
}

void Session::Start() {
  auto self = shared_from_this();

  socket_.AsyncRead([&, self](std::vector<char>& data, ssize_t len) {
    if (len == 0)
      io_.Release(socket_.Raw());

    else
      Reply(std::string(data.data(), static_cast<size_t>(len)));
  });
}

void Session::Reply(const std::string& input) {
  std::string reply;

  parser_.MakeReply(input, reply);

  if (!reply.empty())
    socket_.Send(reply);
}
