#include "udp_channel.h"

#include <arpa/inet.h>

UdpChannel::UdpChannel(Io& io, const Options& opts) : io_(io), socket_(io) {
  socket_.Open();
  socket_.SetNonBlocking();

  addr_.sin_family = AF_INET;
  addr_.sin_port = htons(opts.GetPort());

  if (inet_pton(AF_INET, opts.GetIP().c_str(), &addr_.sin_addr) <= 0)
    throw std::runtime_error("bad address");

  Bind();
}

void UdpChannel::Bind() {
  if (bind(socket_.Raw(), (sockaddr*)&addr_, sizeof(addr_)) < 0)
    throw std::runtime_error("Bind failed");
}

void UdpChannel::Start() {
  socket_.AsyncRead(
      [&](std::vector<char>& data, ssize_t len, const sockaddr* client, socklen_t socklen) {
        if (len == 0)
          io_.Release(socket_.Raw());

        else
          Reply(std::string(data.data(), static_cast<size_t>(len)), client, socklen);
      });
}

void UdpChannel::Reply(const std::string& input, const sockaddr* client, socklen_t socklen) {
  std::string reply;

  parser_.MakeReply(input, reply);

  if (!reply.empty())
    socket_.Send(reply, client, socklen);
}
