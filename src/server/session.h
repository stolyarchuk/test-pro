#ifndef SESSION_H
#define SESSION_H

#include <memory>

#include "common/io.h"
#include "common/parser.h"
#include "common/tcp_socket.h"

class Session : public std::enable_shared_from_this<Session> {
 public:
  Session(Io&, TcpSocket&&);
  ~Session() {
    socket_.Close();
  }

  void Start();

 private:
  Io& io_;
  TcpSocket socket_;
  Parser parser_;

  void Reply(const std::string&);
};

#endif  // SESSION_H
