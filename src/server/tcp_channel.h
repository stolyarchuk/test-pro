#pragma once

#include "acceptor.h"
#include "base_channel.h"
#include "common/io.h"
#include "common/options.h"

class TcpChannel : public Channel {
 public:
  using Ptr = std::unique_ptr<TcpChannel>;

  TcpChannel(Io&, const Options&);

  void Start() override;

 private:
  Io& io_;
  Acceptor acceptor_;
};
