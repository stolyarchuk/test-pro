#ifndef CHANNEL_H
#define CHANNEL_H

#include <memory>

class Channel {
 public:
  using Ptr = std::unique_ptr<Channel>;

  Channel();
  virtual ~Channel();

  virtual void Start() = 0;
};

#endif  // CHANNEL_H
