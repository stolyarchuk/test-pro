#ifndef TCP_SOCKET_H
#define TCP_SOCKET_H

#include "base_socket.h"

class TcpSocket : public Socket {
 public:
  using Socket::Socket;
  void Open() override;
  void AsyncRead(TcpReadHandler::Func&& f);
  void Send(const std::string& data);
  void Connect(const std::string&, uint16_t);
};

#endif  // TCP_SOCKET_H
