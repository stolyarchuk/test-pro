#include "io.h"

#include <unistd.h>

#include <sys/eventfd.h>
#include <csignal>
#include <iostream>
#include <stdexcept>

Io::Io(Queue& queue) : queue_(queue), stopped_(false), ev_list_(64) {
  epoll_fd_ = epoll_create1(0);

  if (epoll_fd_ == -1)
    throw std::runtime_error("Failed to create epoll file descriptor");

  close_fd_ = eventfd(0, EFD_NONBLOCK);

  if (close_fd_ == -1)
    throw std::runtime_error("Failed to create close event file descriptor");

  epoll_event close_event;

  close_event.data.fd = close_fd_;
  close_event.events = EPOLLIN | EPOLLET;

  if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, close_fd_, &close_event) != 0)
    throw std::runtime_error("Epoll add fd");
}

Io::~Io() {
  close(epoll_fd_);
}

void Io::Run() {
  while (!stopped_) {
    int max_events = static_cast<int>(ev_list_.size());
    int ready = epoll_wait(epoll_fd_, &ev_list_[0], max_events, -1);

    if (ready == -1) {
      throw std::runtime_error("Io::Run()::epoll_wait");
    }

    for (size_t i = 0; i < static_cast<size_t>(ready); i++) {
      auto ev = ev_list_[i];
      int fd = ev.data.fd;

      if (fd == close_fd_ && ev.events & EPOLLIN) {
        std::cout << "Stopping epoll" << std::endl;

        //        for (const auto& handler_p : handlers_)
        //          Release(handler_p.first);

        stopped_ = true;
      }

      if (handlers_.count(fd)) {
        auto handler = handlers_[fd];

        if (handler != nullptr) {
          auto fut = std::async(std::launch::deferred, [handler, &ev] { handler->Exec(std::move(ev)); });
          queue_.Add(std::move(fut));
        }
      }
    }
  }
}

void Io::Stop() {
  eventfd_write(close_fd_, 1);
}

void Io::Register(Handler::Ptr handler) {
  auto& event = handler->event;

  int err = epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, event.data.fd, &event);

  if (err != 0)
    throw std::runtime_error("Epoll add fd");

  handlers_[event.data.fd] = handler;
}

void Io::Release(int fd) {
  int err = epoll_ctl(epoll_fd_, EPOLL_CTL_DEL, fd, nullptr);

  if (err != 0)
    throw std::runtime_error("Epoll del fd");

  if (handlers_.count(fd))
    handlers_.erase(fd);
}
