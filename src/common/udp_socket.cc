#include "udp_socket.h"

#include <netinet/in.h>
#include <sys/socket.h>
#include <thread>

void UdpSocket::Open() {
  socket_fd_ = socket(AF_INET, SOCK_DGRAM, 0);

  if (socket_fd_ < 0)
    throw std::runtime_error("UdpSocket::Open()::socket");

  int opts;

  int err = setsockopt(socket_fd_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opts, sizeof(opts));

  if (err != 0)
    throw std::runtime_error("UdpSocket::Open()::setsockopt");
}

void UdpSocket::AsyncRead(UdpReadHandler::Func&& f) {
  auto handler = std::make_shared<UdpReadHandler>(std::move(f));

  handler->event.events = EPOLLIN | EPOLLERR | EPOLLET;
  handler->event.data.fd = socket_fd_;

  io_.Register(handler);
}

void UdpSocket::Send(const std::string& data, const sockaddr* client, socklen_t socklen) {
  std::lock_guard<std::mutex> locker(mutex_send_);
  sendto(socket_fd_, data.data(), data.size(), MSG_CONFIRM, client, socklen);
}
