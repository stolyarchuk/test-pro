#ifndef HANDLER_H
#define HANDLER_H

#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <unistd.h>

#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <vector>

class Socket;

struct Handler {
  enum class Type { kNone, kSignal, kAccept, kTcpRead, kUdpRead, kConsole };
  using Ptr = std::shared_ptr<Handler>;

  virtual ~Handler();
  virtual void Exec(epoll_event&&) = 0;
  Type GetType() const {
    return type_;
  }

  epoll_event event;

 protected:
  Type type_ = Type::kNone;
  std::mutex mutex_op_;
};

struct SignalsHandler : public Handler {
  using Ptr = std::shared_ptr<SignalsHandler>;
  using Func = std::function<void(uint32_t)>;

  SignalsHandler(Func&& f);

  virtual void Exec(epoll_event&& event) override;

 private:
  Func handler;
};

struct AcceptHandler : public Handler {
  using Ptr = std::shared_ptr<AcceptHandler>;
  using Func = std::function<void(int)>;

  AcceptHandler(Func&& f);

  virtual void Exec(epoll_event&& event) override;

 private:
  Func handler;
};

struct TcpReadHandler : public Handler {
  using Ptr = std::shared_ptr<TcpReadHandler>;
  using Func = std::function<void(std::vector<char>&, ssize_t)>;

  TcpReadHandler(Func&&);

  virtual void Exec(epoll_event&& event) override;

 private:
  Func handler;
};

struct UdpReadHandler : public Handler {
  using Ptr = std::shared_ptr<UdpReadHandler>;
  using Func = std::function<void(std::vector<char>&, ssize_t, const sockaddr*, socklen_t)>;

  UdpReadHandler(Func&&);

  virtual void Exec(epoll_event&& event) override;

 private:
  Func handler;
};

struct ConsoleHandler : public Handler {
  using Ptr = std::shared_ptr<ConsoleHandler>;
  using Func = std::function<void(const std::string&)>;

  ConsoleHandler(Func&& f);

  virtual void Exec(epoll_event&& event) override;

 private:
  Func handler;
};

#endif  // HANDLER_H
