#ifndef QUEUE_H
#define QUEUE_H

#include <atomic>
#include <condition_variable>
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <thread>

class Queue {
 public:
  using Fut = std::future<void>;

  Queue();
  ~Queue();

  void Start();
  void Stop();
  void Add(Fut&&);

 private:
  std::mutex lock_;
  std::vector<std::thread> threads_;
  std::queue<Fut> q_;
  std::condition_variable cv_;
  bool stopped_;

  void Wait();
};

#endif  // QUEUE_H
