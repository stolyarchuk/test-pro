#include "base_socket.h"

#include <fcntl.h>

Socket::Socket(Io& io) : io_(io) {
}

Socket::Socket(Io& io, int socket_fd) : io_(io), socket_fd_(socket_fd) {
}

Socket::~Socket() {
}

void Socket::SetNonBlocking() {
  int opts;

  opts = fcntl(socket_fd_, F_GETFL);

  if (opts < 0)
    throw std::runtime_error("Socket::SetNonBlocking()::fcntl(F_GETFL)");

  opts = (opts | O_NONBLOCK);

  if (fcntl(socket_fd_, F_SETFL, opts) < 0)
    throw std::runtime_error("Socket::SetNonBlocking()::fcntl(F_SETFL)");
}

void Socket::Close() {
  close(socket_fd_);
}

int Socket::Raw() const {
  return socket_fd_;
}
