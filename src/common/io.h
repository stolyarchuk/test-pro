#ifndef IO_H
#define IO_H

#include <atomic>
#include <iostream>
#include <map>
#include <memory>
#include <vector>

#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <unistd.h>

#include "handler.h"
#include "queue.h"

class Io {
 public:
  Io(Queue&);
  ~Io();

  void Run();
  void Stop();

  void Register(Handler::Ptr handler);
  void Release(int);

 private:
  Queue& queue_;

  int epoll_fd_;
  int close_fd_;
  bool stopped_;

  std::vector<epoll_event> ev_list_;
  std::map<int, Handler::Ptr> handlers_;
};

#endif  // IO_H
