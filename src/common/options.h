#ifndef OPTIONS_H
#define OPTIONS_H

#include <iostream>
#include <map>
#include <string>

#include "base_mode.h"

class Options {
 public:
  Options(int argc, char** argv);

  static const std::map<Mode, std::string> Modes;
  static const std::map<Proto, std::string> Protos;

  void Parse();

  Mode GetMode() const;
  Proto GetProto() const;
  uint16_t GetPort() const;
  std::string GetIP() const;

  void Throw(const std::string& e) const;

  friend std::ostream& operator<<(std::ostream& os, const Options& O) {
    return os << "Usage: " << O.name_ << " [<switches>...] [<options>...]\n\n"
              << "<switches>\n"
              << "\t-s/-c\t: Operational mode (server/client)\n"
              << "\t-t/-u\t: Protocol to communicate (tcp/udp)\n"
              << "<options>\n"
              << "\t-a [ip] : IP address to bind in server mode or to connect to in client one "
                 "(default: 127.0.0.1)\n"
              << "\t-p [num]: Port number to listen in server mode or to connect to in client one "
                 "(default: 5555)\n";
  }

 private:
  int argc_;
  char** argv_;

  std::string name_;

  Mode mode_;
  Proto proto_;
  uint16_t port_;
  std::string ip_;
};

#endif  // OPTIONS_H
