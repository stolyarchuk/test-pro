#include "signal_set.h"

#include <iostream>

void SignalSet::AsyncWait(SignalsHandler::Func&& f) {
  auto handler = std::make_shared<SignalsHandler>(std::move(f));

  handler->event.events = EPOLLIN | EPOLLET;
  handler->event.data.fd = signal_fd_;

  io_.Register(handler);
}
