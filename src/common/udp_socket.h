#ifndef UDP_SOCKET_H
#define UDP_SOCKET_H

#include <mutex>

#include "base_socket.h"

class UdpSocket : public Socket {
 public:
  using Socket::Socket;

  void Open() override;
  void AsyncRead(UdpReadHandler::Func&& f);
  void Send(const std::string&, const sockaddr*, socklen_t);

 private:
  std::mutex mutex_send_;
};

#endif  // UDP_SOCKET_H
