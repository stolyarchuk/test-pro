#ifndef IBASE_H
#define IBASE_H

#include <sys/socket.h>
#include <memory>

enum class Mode { None = 0, Server, Client };
enum class Proto { NONE = 0, TCP = SOCK_STREAM, UDP = SOCK_DGRAM };

class BaseMode {
 public:
  using Ptr = std::unique_ptr<BaseMode>;

  BaseMode();
  virtual ~BaseMode();

  virtual void Start() = 0;
};

#endif  // IBASE_H
