#include "handler.h"

#include <netinet/in.h>
#include <string>
#include <thread>

Handler::~Handler() {
}

SignalsHandler::SignalsHandler(Func&& f) : handler(std::move(f)) {
  type_ = Type::kSignal;
}

void SignalsHandler::Exec(epoll_event&& event) {
  std::cout << "Thread " << std::this_thread::get_id() << ": SignalsHandler" << std::endl;

  int fd = event.data.fd;
  signalfd_siginfo info;

  ssize_t len = read(fd, &info, sizeof(signalfd_siginfo));

  if (len != sizeof(signalfd_siginfo))
    throw std::runtime_error("SignalsHandler");

  handler(info.ssi_signo);
}

AcceptHandler::AcceptHandler(Func&& f) : handler(std::move(f)) {
  type_ = Type::kAccept;
}

void AcceptHandler::Exec(epoll_event&& event) {
  std::cout << "Thread " << std::this_thread::get_id() << ": AcceptHandler" << std::endl;

  int fd = event.data.fd;
  int session_fd = -1;
  sockaddr_in remote_addr;
  socklen_t session = sizeof(remote_addr);

  if (event.events | EPOLLERR) {
    std::lock_guard<std::mutex> lock(mutex_op_);
    session_fd = accept(fd, (sockaddr*)&remote_addr, &session);
  }
  if (session_fd < 0)
    throw std::runtime_error("AcceptHandler");

  handler(session_fd);
}

TcpReadHandler::TcpReadHandler(Func&& f) : handler(std::move(f)) {
  type_ = Type::kTcpRead;
}

void TcpReadHandler::Exec(epoll_event&& event) {
  std::cout << "Thread " << std::this_thread::get_id() << ": TcpReadHandler" << std::endl;

  int fd = event.data.fd;
  ssize_t len = 0;
  std::vector<char> data(1024);

  if (event.events | EPOLLERR) {
    std::lock_guard<std::mutex> lock(mutex_op_);
    len = recv(fd, &data[0], data.size(), MSG_WAITALL);
  }

  handler(data, len);
}

UdpReadHandler::UdpReadHandler(Func&& f) : handler(std::move(f)) {
  type_ = Type::kUdpRead;
}

void UdpReadHandler::Exec(epoll_event&& event) {
  std::cout << "Thread " << std::this_thread::get_id() << ": UdpReadHandler" << std::endl;

  int fd = event.data.fd;
  ssize_t len = 0;
  std::vector<char> data(1024);
  sockaddr_in cliaddr;
  socklen_t socklen = sizeof(cliaddr);

  if (event.events | EPOLLERR) {
    std::lock_guard<std::mutex> lock(mutex_op_);
    len = recvfrom(fd, &data[0], data.size(), MSG_WAITALL, (sockaddr*)&cliaddr, &socklen);
  }

  handler(data, len, (const sockaddr*)&cliaddr, socklen);
}

ConsoleHandler::ConsoleHandler(Func&& f) : handler(std::move(f)) {
  type_ = Type::kConsole;
}

void ConsoleHandler::Exec(epoll_event&& event) {
  std::cout << "Thread " << std::this_thread::get_id() << ": ConsoleHandler" << std::endl;

  std::string info;

  if (event.events & EPOLLIN)
    getline(std::cin, info);

  handler(info);
}
