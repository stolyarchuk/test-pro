#include "app.h"

App::App(const Options& opts) : io_(queue_), signals_(io_, SIGINT, SIGTERM, SIGQUIT), opts_(opts) {
  signals_.AsyncWait([&](int signal) {
    std::cout << "Got signal: " << signal << std::endl;
    io_.Stop();
  });

  switch (opts.GetMode()) {
    case Mode::Server:
      mode_ = std::make_unique<ServerMode>(io_, opts);
      break;
    case Mode::Client:
      mode_ = std::make_unique<ClientMode>(io_, opts);
      break;
    case Mode::None:
      break;
  }
}

void App::Run() {
  queue_.Start();

  if (!mode_)
    throw std::invalid_argument("Unknown mode");

  mode_->Start();
  PrintSummary();

  io_.Run();
}

void App::PrintSummary() {
  std::cout << "Started in " << Options::Modes.at(opts_.GetMode())               //
            << " mode, using " << Options::Protos.at(opts_.GetProto())           //
            << " protocol, ip:port " << opts_.GetIP() << ':' << opts_.GetPort()  //
            << std::endl;
}
