#ifndef PARSER_H
#define PARSER_H

#include <vector>
#include <string>

class Parser {
 public:
  Parser();

  void MakeReply(const std::string&, std::string&);
};

#endif // PARSER_H
