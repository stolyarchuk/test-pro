#ifndef SOCKET_H
#define SOCKET_H

#include <sys/socket.h>

#include "io.h"

class Socket {
 public:
  Socket(Io& io);
  Socket(Io& io, int);
  virtual ~Socket();

  virtual void Open() = 0;
  void SetNonBlocking();
  void Close();

  int Raw() const;

 protected:
  Io& io_;
  int socket_fd_;
};

#endif  // SOCKET_H
