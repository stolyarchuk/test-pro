#include "options.h"

#include <unistd.h>
#include <iostream>

const std::map<Mode, std::string> Options::Modes{
    {Mode::None, "none"}, {Mode::Server, "server"}, {Mode::Client, "client"}};

const std::map<Proto, std::string> Options::Protos{{Proto::NONE, "NONE"}, {Proto::TCP, "tcp"}, {Proto::UDP, "udp"}};

Options::Options(int argc, char** argv)
    : argc_(argc), argv_(argv),                //
      mode_(Mode::None), proto_(Proto::NONE),  //
      port_(5555), ip_("127.0.0.1") {
  const std::string argv0 = argv[0];
  name_ = argv0.substr(argv0.find_last_of("/") + 1);
}

void Options::Parse() {
  try {
    int opt;
    while ((opt = getopt(argc_, argv_, "sctup:da:s")) != EOF)
      switch (opt) {
        case 's': {
          if (mode_ == Mode::Client)
            Throw("Use client or server mode. Not both.");
          mode_ = Mode::Server;
          break;
        }
        case 'c': {
          if (mode_ == Mode::Server)
            Throw("Use client or server mode. Not both.");
          mode_ = Mode::Client;
          break;
        }
        case 't': {
          if (proto_ == Proto::UDP)
            Throw("Use TCP or UDP protocol. Not both.");
          proto_ = Proto::TCP;
          break;
        }
        case 'u': {
          if (proto_ == Proto::TCP)
            Throw("Use TCP or UDP protocol. Not both.");
          proto_ = Proto::UDP;
          break;
        }
        case 'p':
          port_ = static_cast<uint16_t>(std::stoi(optarg));
          break;
        case 'a':
          ip_ = std::string(optarg);
          break;
      }

    if (mode_ == Mode::None)
      Throw("Select server or client mode (-s/-c)");
    if (proto_ == Proto::NONE)
      Throw("Select TCP or UDP protocol (-t/-u)");
  } catch (std::exception& e) {
    Throw(e.what());
  }
}

Mode Options::GetMode() const {
  return mode_;
}

Proto Options::GetProto() const {
  return proto_;
}

uint16_t Options::GetPort() const {
  return port_;
}

std::string Options::GetIP() const {
  return ip_;
}

void Options::Throw(const std::string& e) const {
  throw std::invalid_argument(e);
}
