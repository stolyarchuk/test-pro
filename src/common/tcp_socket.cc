#include "tcp_socket.h"

#include <arpa/inet.h>
#include <netinet/in.h>

void TcpSocket::Open() {
  socket_fd_ = socket(AF_INET, SOCK_STREAM, 0);

  if (socket_fd_ < 0)
    throw std::runtime_error("TcpSocket::Open()::socket");

  int opts = -1;

  int err = setsockopt(socket_fd_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opts, sizeof(opts));

  if (err != 0)
    throw std::runtime_error("TcpSocket::Open()::setsockopt");
}

void TcpSocket::AsyncRead(TcpReadHandler::Func&& f) {
  auto handler = std::make_shared<TcpReadHandler>(std::move(f));

  handler->event.events = EPOLLIN | EPOLLERR | EPOLLET;
  handler->event.data.fd = socket_fd_;

  io_.Register(handler);
}

void TcpSocket::Send(const std::string& data) {
  send(socket_fd_, data.data(), data.size(), 0);
}

void TcpSocket::Connect(const std::string& ip, uint16_t port) {
  sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr) <= 0)
    throw std::runtime_error("Bad address");

  if (connect(socket_fd_, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    throw std::runtime_error("Connection failed");
}
