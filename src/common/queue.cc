#include "queue.h"

#include <iostream>
#include <stdexcept>

Queue::Queue() : stopped_(false) {
}

Queue::~Queue() {
  if (!stopped_)
    Stop();

  for (auto& thread : threads_) {
    if (thread.joinable())
      thread.join();
  }
}

void Queue::Add(Fut&& fut) {
  {
    std::unique_lock<std::mutex> lock(lock_);
    q_.push(std::move(fut));
  }
  cv_.notify_all();
}

void Queue::Start() {
  unsigned cpu_count = std::thread::hardware_concurrency();

  if (cpu_count <= 1)
    cpu_count = 1;
  else
    cpu_count /= 2;

  for (size_t i = 0; i < cpu_count; i++) {
    threads_.push_back(std::thread(&Queue::Wait, this));
  }
}

void Queue::Stop() {
  std::cout << "Stopping queue" << std::endl;

  stopped_ = true;
  cv_.notify_all();
}

void Queue::Wait() {
  std::unique_lock<std::mutex> lock(lock_);

  while (!stopped_) {
    cv_.wait(lock, [this] { return (!q_.empty() || stopped_); });

    if (!q_.empty()) {
      auto op = std::move(q_.front());
      q_.pop();

      lock.unlock();
      op.get();
      lock.lock();
    }
  }
}
