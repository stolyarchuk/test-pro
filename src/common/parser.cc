#include "parser.h"

#include <numeric>
#include <regex>

Parser::Parser() {
}

void Parser::MakeReply(const std::string& data, std::string& reply) {
  std::vector<int> nums;
  std::regex r("\\d+");

  std::sregex_iterator next(data.begin(), data.end(), r);
  std::sregex_iterator end;

  while (next != end) {
    std::smatch match = *next;
    nums.push_back(std::stoi(match.str()));
    next++;
  }

  if (!nums.empty()) {
    std::sort(nums.begin(), nums.end());
    int total = std::accumulate(nums.begin(), nums.end(), 0);

    std::ostringstream os;

    for (auto it = nums.begin(); it != nums.end(); it++) {
      if (it != nums.begin())
        os << ' ';

      os << *it;
    }

    os << std::endl << total;

    reply = os.str();
  }
}
