#ifndef APP_H
#define APP_H

#include "client/client_mode.h"
#include "io.h"
#include "options.h"
#include "queue.h"
#include "server/server_mode.h"
#include "signal_set.h"

class App {
 public:
  App(const Options& opts);

  void Run();

  void PrintSummary();

 private:
  Queue queue_;

  Io io_;
  SignalSet signals_;

  BaseMode::Ptr mode_;
  const Options& opts_;
};

#endif  // APP_H
