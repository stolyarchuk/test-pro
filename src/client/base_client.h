#ifndef CLIENT_H
#define CLIENT_H

#include <memory>

#include "common/tcp_socket.h"

class Client {
 public:
  using Ptr = std::unique_ptr<Client>;

  Client(Io& io);
  virtual ~Client();

  virtual void Start() = 0;
  virtual void Send(const std::string&) = 0;
};

#endif  // CLIENT_H
