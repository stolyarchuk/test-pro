#ifndef CLIENT_MODE_H
#define CLIENT_MODE_H

#include "common/base_mode.h"
#include "common/io.h"
#include "common/options.h"
#include "tcp_client.h"
#include "udp_client.h"

class ClientMode : public BaseMode {
 public:
  using Ptr = std::unique_ptr<ClientMode>;

  ClientMode(Io&, const Options&);

  void Start() override;

 private:
  Client::Ptr client_;
};

#endif  // CLIENT_MODE_H
