#ifndef UDP_CLIENT_H
#define UDP_CLIENT_H

#include <netinet/ip.h>
#include <sys/socket.h>

#include "base_client.h"
#include "common/base_mode.h"
#include "common/io.h"
#include "common/options.h"
#include "common/udp_socket.h"

class UdpClient : public Client {
 public:
  using Ptr = std::unique_ptr<UdpClient>;
  UdpClient(Io&, const Options&);

  void Start();
  void Send(const std::string&);

 private:
  Io& io_;
  UdpSocket socket_;
  sockaddr_in addr_;
};

#endif  // UDP_CLIENT_H
