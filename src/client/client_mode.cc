#include "client_mode.h"

ClientMode::ClientMode(Io& io, const Options& opts) {
  switch (opts.GetProto()) {
    case Proto::TCP:
      client_ = std::make_unique<TcpClient>(io, opts);
      break;
    case Proto::UDP:
      client_ = std::make_unique<UdpClient>(io, opts);
      break;
    case Proto::NONE:
      break;
  }
}

void ClientMode::Start() {
  if (client_)
    client_->Start();
}
