#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <netinet/ip.h>
#include <sys/socket.h>

#include "base_client.h"
#include "common/io.h"
#include "common/options.h"
#include "common/tcp_socket.h"

class TcpClient : public Client {
 public:
  using Ptr = std::unique_ptr<TcpClient>;
  TcpClient(Io& io, const Options& opts);

  void Start();
  void Send(const std::string&);

 private:
  Io& io_;
  TcpSocket socket_;
  sockaddr_in addr_;
};

#endif  // TCP_CLIENT_H
