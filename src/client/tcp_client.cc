#include "tcp_client.h"

TcpClient::TcpClient(Io& io, const Options& opts) : Client(io), io_(io), socket_(io) {
  socket_.Open();
  socket_.Connect(opts.GetIP(), opts.GetPort());
  socket_.SetNonBlocking();
}

void TcpClient::Start() {
  socket_.AsyncRead([&](std::vector<char>& data, ssize_t len) {
    if (len == 0)
      io_.Release(socket_.Raw());

    else
      std::cout << data.data() << std::endl;
  });
}

void TcpClient::Send(const std::string& input) {
  socket_.Send(input);
}
