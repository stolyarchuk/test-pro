#include "udp_client.h"

#include <arpa/inet.h>

UdpClient::UdpClient(Io& io, const Options& opts) : Client(io), io_(io), socket_(io) {
  socket_.Open();
  socket_.SetNonBlocking();

  addr_.sin_family = AF_INET;
  addr_.sin_port = htons(opts.GetPort());

  if (inet_pton(AF_INET, opts.GetIP().c_str(), &addr_.sin_addr) <= 0)
    throw std::runtime_error("Bad address");
}

void UdpClient::Send(const std::string& input) {
  socket_.Send(input, (const sockaddr*)&addr_, sizeof(addr_));
}

void UdpClient::Start() {
  socket_.AsyncRead(
      [&](std::vector<char>& data, ssize_t len, const sockaddr* /*client*/, socklen_t /*socklen*/) {
        if (len == 0)
          io_.Release(socket_.Raw());

        else
          std::cout << data.data() << std::endl;
      });
}
