#include "base_client.h"

#include <sstream>

Client::Client(Io& io) {
  auto f = [&](const std::string& data) {
    if (!data.empty()) {
      Send(data);
    }
  };

  auto handler = std::make_shared<ConsoleHandler>(std::move(f));

  handler->event.events = EPOLLIN | EPOLLET;
  handler->event.data.fd = STDIN_FILENO;

  io.Register(handler);
}

Client::~Client() {
}
