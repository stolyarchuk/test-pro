#include <iostream>

#include "common/app.h"
#include "common/options.h"

int main(int argc, char** argv) {
  int rc = EXIT_SUCCESS;

  Options opts(argc, argv);

  try {
    opts.Parse();

    App app(opts);
    app.Run();

  } catch (std::invalid_argument& e) {
    std::cout << e.what() << "\n\n" << opts << std::endl;

  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;

    rc = EXIT_FAILURE;
  }

  return rc;
}
